package org.aksw.openqa.server.servlet;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.IResultMapList;
import org.aksw.openqa.manager.plugin.PluginManager;
import org.aksw.openqa.manager.plugin.PluginManagerPool;
import org.aksw.openqa.server.ServerEnviromentVariables;
import org.aksw.openqa.server.util.ServerSearchUtil;
import org.apache.log4j.Logger;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
@WebServlet(name = "api/rest/search", urlPatterns = { "/api/rest/search" })
public class SearchServlet extends HttpServlet {

	public final static String OUTPUT_CONTENT_PARAM = "content";
	public final static String OUTPUT_CONTENT_PARAM_TYPE_SPARQL = "sparql";
	public final static String INPUT_QUERY_PARAM = "q";

	/**
	 * 
	 */
	private static final long serialVersionUID = -5755833016211618969L;
	private static Logger logger = Logger.getLogger(SearchServlet.class);

	@Override
	public void init() throws ServletException {
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		logger.info("Starting a new openQA Call");
		Map<String, Object> servletParams = new HashMap<String, Object>();
		Enumeration<String> parameters = req.getParameterNames();
		while (parameters.hasMoreElements()) {
			String parameterName = parameters.nextElement();
			String parameter = req.getParameter(parameterName);
			servletParams.put(parameterName, parameter);
			logger.info("Processing param: " 
					+ parameterName + " = "
					+ parameter);
		}

		// setting up the context
		ServletOutputStream out = resp.getOutputStream();
		PluginManagerPool pool = (PluginManagerPool) ServerEnviromentVariables.getInstance().getParam(ServerEnviromentVariables.PLUGIN_MANAGER_POOL);
		PluginManager pluginManager = null;
		try {
			pluginManager = pool.borrowObject();
			IResultMapList<? extends IResultMap> result = ServerSearchUtil.search(servletParams, req, pluginManager);
			out.print(result.toJSON());
		} catch (Exception e) {
			logger.error("Exception executing search.", e);
		} finally {
			if(pluginManager != null) {
				pool.returnObject(pluginManager);
			}
		}
	}
}
