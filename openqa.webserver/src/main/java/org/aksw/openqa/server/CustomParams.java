package org.aksw.openqa.server;

import java.io.File;
import java.io.Serializable;
import java.util.concurrent.ConcurrentNavigableMap;

import org.mapdb.DB;
import org.mapdb.DBMaker;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class CustomParams implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8598003281170345448L;
	String context;
	String path;
	
	public CustomParams(String path, String context) {
		this.context = context;
		this.path = path;
	}
	
	public synchronized String getProperty(String property, String defaultValue) {
		DB db = getDB();
		String value;
		try {
			value = getMap(db).get(property);
			if(value == null) {
				return defaultValue;
			}
			return value;
		} finally  {
			db.close();
		}
	}
	
	public synchronized void setProperty(String property, String value) {
		DB db = getDB();
		try {
			getMap(db).put(property, value);
			db.commit();
		} finally  {
			db.close();
		}		
	}
	
	private ConcurrentNavigableMap<String, String> getMap(DB db) {
		ConcurrentNavigableMap<String, String> treeMap = db.getTreeMap(context);
		return treeMap;
	}
	
	private DB getDB() {
		File file = new File(path + "/" + context + "_custom.prop");
		DB db = DBMaker.newFileDB(file).make();
		return db;
	}
}
