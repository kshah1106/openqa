package org.aksw.openqa.server;

import org.aksw.openqa.component.service.cache.impl.CacheService;
import org.aksw.openqa.manager.plugin.PluginManagerPool;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class PluginManagerPoolInstanciator {
	
	public static PluginManagerPoolInstanciator pluginManagerPoolInstanciator;
	
	String pluginDir;
	String appContext;
	ClassLoader contextClassLoader;	
	CacheService cacheService;
	
	public PluginManagerPoolInstanciator(String pluginDir,
			ClassLoader contextClassLoader, 
			String appContext,
			CacheService cacheService) {
		this.pluginDir = pluginDir;
		this.contextClassLoader = contextClassLoader;
		this.appContext = appContext;
		this.cacheService = cacheService;
	}

	public synchronized PluginManagerPool newInstance() {
		return new PluginManagerPool(pluginDir, 
				contextClassLoader, 
				appContext, 
				cacheService);
	}

}
