package org.aksw.openqa.server.view.controller;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.Properties;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.aksw.openqa.server.CustomParams;
import org.aksw.openqa.server.ServerEnviromentVariables;
import org.aksw.openqa.server.util.MD5Util;
import org.apache.log4j.Logger;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
@ManagedBean(name="applicationViewController")
@SessionScoped
public class ApplicationViewController implements Serializable {
	
	private static Logger logger = Logger.getLogger(ApplicationViewController.class);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2415768995559873799L;
	private String logo = "resources/images/logo.png";
	private String name = "openQA :)";
	private String user = "admin";
	private String pass = "admin";
	
	private static final String CONTEXT_ID  = "webserver.app";
	
	private static final String APPLICATION_NAME_PARAM = "app.name";
	private static final String APPLICATION_LOGO_PARAM = "app.logo";
	private static final String APPLICATION_USER_PARAM = "app.user";
	private static final String APPLICATION_PASS_PARAM = "app.pass";
	
	private static final String APP_PROPERTIES_FILE = "application.properties";
	
	private CustomParams customParams = null;
	private Properties prop = new Properties();

	public ApplicationViewController() {
		try {			
			InputStream inStream = ApplicationViewController.this.getClass().getResourceAsStream("/" + APP_PROPERTIES_FILE); // loading component info file			
			if(inStream != null) {
				InputStreamReader isr = new InputStreamReader(inStream, "UTF-8");
				prop.load(isr);
				logo = prop.getProperty(APPLICATION_LOGO_PARAM, logo);
				name = prop.getProperty(APPLICATION_NAME_PARAM, name);
				user = prop.getProperty(APPLICATION_USER_PARAM, user);
				pass = prop.getProperty(APPLICATION_PASS_PARAM, pass);
				pass = MD5Util.encode(pass);
				inStream.close();
			}
			
			String appContextDir = (String) ServerEnviromentVariables.getInstance().
					getParam(ServerEnviromentVariables.CONTEXT_DIR);
			
			customParams = new CustomParams(appContextDir, CONTEXT_ID);
			logo = customParams.getProperty(APPLICATION_LOGO_PARAM, logo);
			name = customParams.getProperty(APPLICATION_NAME_PARAM, name);
			user = customParams.getProperty(APPLICATION_USER_PARAM, user);
			pass = customParams.getProperty(APPLICATION_PASS_PARAM, pass);
		} catch (Exception e) {
			logger.error("Error initializing application properties.", e);
		}
	}
	
	public String getLogo() {
		return logo;
	}
	
	public void setLogo(String logo) {
		if(logo != null && !logo.equals(this.logo)) {
			this.logo = logo;
			customParams.setProperty(APPLICATION_LOGO_PARAM, logo);
		}
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		if(name != null && !name.equals(this.name)) {
			this.name = name;
			customParams.setProperty(APPLICATION_NAME_PARAM, name);
		}
	}
	
	public String getUser() {
		return user;
	}
	
	public void setUser(String user) {
		if(user != null && !user.equals(this.user)) {
			this.user = user;
			customParams.setProperty(APPLICATION_USER_PARAM, user);
		}
	}
	
	public String getPass() {
		return pass;
	}
	
	public void setPass(String pass) {
		if(pass != null && !pass.equals(this.pass)) {
			this.pass = pass;
			customParams.setProperty(APPLICATION_PASS_PARAM, pass);
		}
	}
}
