package org.aksw.openqa.server.util;

import java.util.Iterator;

import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.ResultMap;
import org.aksw.openqa.component.param.ResultMapList;
import org.json.JSONArray;
import org.json.JSONException;

public class ResultUtil {

	public static ResultMapList<IResultMap> toResult(JSONArray array) {
		ResultMapList<IResultMap> resultList = new ResultMapList<IResultMap>();
		try {
			for(int i = 0 ; i < array.length() ; i++){
			    @SuppressWarnings("unchecked")
				Iterator<String> keys = array.getJSONObject(i).keys();
			    ResultMap result = new ResultMap();
			    while(keys.hasNext()) {
			    	String key = keys.next();
			    	String value = (String) array.getJSONObject(i).get(key);
			    	result.setParam(key, value);
			    }
			    resultList.add(result);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}		
		return resultList;
	}	
}
