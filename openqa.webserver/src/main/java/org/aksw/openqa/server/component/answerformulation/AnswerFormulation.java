package org.aksw.openqa.server.component.answerformulation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.param.IParamMap;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.IResultMapList;
import org.aksw.openqa.component.param.ParamMap;
import org.aksw.openqa.component.providers.impl.QueryParserProvider;
import org.aksw.openqa.component.providers.impl.RetrieverProvider;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.aksw.openqa.component.providers.impl.SynthesizerProvider;
import org.aksw.openqa.manager.plugin.PluginManager;
import org.apache.log4j.Logger;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class AnswerFormulation {
	
	private static Logger logger = Logger.getLogger(AnswerFormulation.class);

	public IResultMapList<? extends IResultMap> process(boolean skipRetriever, Map<String, Object> params,
			PluginManager pluginManager,
			IContext context) throws Exception {

		ServiceProvider serviceProvider = (ServiceProvider) pluginManager.getProvider(ServiceProvider.class);
		ParamMap token = new ParamMap();
		
		logger.info("Parameterizing");		
		// param set
		for(Entry<String, Object> entry : params.entrySet()) {
			token.setParam(entry.getKey(), entry.getValue());
		}
		
		logger.debug("Parsing");
		QueryParserProvider interpreterProvider = (QueryParserProvider) pluginManager.getProvider(QueryParserProvider.class);
		List<IParamMap> arguments = new ArrayList<IParamMap>();
		arguments.add(token);
		IResultMapList<? extends IResultMap> parsedQueries = interpreterProvider.process(arguments, serviceProvider, context);
		logger.debug("Number of parsed queries: " + parsedQueries.size());

		if(skipRetriever) {
			return parsedQueries;
		}
		
		logger.debug("Retrieving");
		RetrieverProvider retrieverProvider = (RetrieverProvider) pluginManager.getProvider(RetrieverProvider.class);
		IResultMapList<? extends IResultMap> retrieverResults = retrieverProvider.process(parsedQueries, serviceProvider, context);	
		logger.debug("Number of retrieved results: " + retrieverResults.size());
		
		logger.debug("Synthesizing");
		SynthesizerProvider synthesizerProvider = (SynthesizerProvider) pluginManager.getProvider(SynthesizerProvider.class);
		IResultMapList<? extends IResultMap> synthesis = synthesizerProvider.process(retrieverResults, serviceProvider, context);
		logger.debug("Number of synthesis: " + synthesis.size());		
			
		return synthesis;
	}
}
