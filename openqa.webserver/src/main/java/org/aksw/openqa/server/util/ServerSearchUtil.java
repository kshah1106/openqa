package org.aksw.openqa.server.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.aksw.openqa.Properties;
import org.aksw.openqa.Stage;
import org.aksw.openqa.Status;
import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.context.impl.Context;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.IResultMapList;
import org.aksw.openqa.component.providers.impl.ContextProvider;
import org.aksw.openqa.main.QueryResult;
import org.aksw.openqa.manager.plugin.PluginManager;
import org.aksw.openqa.server.PreDefinedQueries;
import org.aksw.openqa.server.component.answerformulation.SPARQLAnswerFormulationPipeline;
import org.aksw.openqa.server.servlet.SearchServlet;
import org.apache.log4j.Logger;

public class ServerSearchUtil {
	
	private static Logger logger = Logger.getLogger(ServerSearchUtil.class);
	
	private static List<Stage> SPARQL_PIPELINE = Collections.synchronizedList(new SPARQLAnswerFormulationPipeline());
	
	public static IResultMapList<? extends IResultMap> search(Map<String, Object> params, Context context, PluginManager pluginManager) {	 

		String outputAbs = (String) params
				.get(SearchServlet.OUTPUT_CONTENT_PARAM);
		String inputQuery = (String) params
				.get(SearchServlet.INPUT_QUERY_PARAM);
		
		// check if the query is pre-defined		
		IResultMapList<? extends IResultMap> result = PreDefinedQueries.getInstance().getQuery(inputQuery, outputAbs);
		if(result != null) {
			return result;
		}

		// create the input params
		Map<String, Object> inputParams = new HashMap<String, Object>();
		if (inputQuery != null) {
			if (context != null) {
				context.setInputQuery(inputQuery);
			}
			inputParams.put(Properties.Literal.TEXT,
					inputQuery); // add the input query
		}
		boolean sparqlProcessStages = false;
		// skip the retrieval stage in case that a SPARQL query must be
		// rendered
		if (outputAbs != null
				&& outputAbs.toLowerCase()
						.equals(SearchServlet.OUTPUT_CONTENT_PARAM_TYPE_SPARQL)) {
			sparqlProcessStages = true;
		}

		// increasing number of queries
		int numberOfQueries = Status.getNumberOfQueries() + 1;
		Status.setNumberOfQueries(numberOfQueries);

		try {
			org.aksw.openqa.AnswerFormulation answerFormulation = new org.aksw.openqa.AnswerFormulation();	
			QueryResult queryResult = null;
			synchronized (pluginManager) {
				if(!sparqlProcessStages) {
					queryResult = answerFormulation.process(inputParams, pluginManager,
						context);
				} else {
					queryResult = answerFormulation.process(inputParams, pluginManager,
							context, SPARQL_PIPELINE);
				}
			}
			result = queryResult.getOutput();
			if (result.size() == 0) {
				// increasing number of queries without result
				int numberOfQueriesWithoutResult = Status
						.getQueriesWithourResult() + 1;
				Status.setQueriesWithourResult(numberOfQueriesWithoutResult);
			}
			return result;
		} catch (Exception e) {
			int numberOfErrors = Status.getNumberOfErrors() + 1;
			Status.setNumberOfErrors(numberOfErrors);
			logger.error("Error processing params", e);
		}		
		return null;
	}
	
	public static IResultMapList<? extends IResultMap> search(Map<String, Object> params, HttpServletRequest req, PluginManager pluginManager) {
		Context context = null;
		try {
			String host = req.getRemoteHost();
			String ip = HTTPUtil.getClientIpAddr(req);
			String user = req.getRemoteUser();
			logger.debug("User ip :" + ip);
			context = pluginManager.getPlugin(ContextProvider.class,
					Context.class);
			context.setContextParam(IContext.REQUEST_HOST, host);
			context.setContextParam(IContext.REQUEST_IP, ip);
			context.setContextParam(IContext.REQUEST_USER, user);
			context.setContextParam(IContext.REQUEST, req);
			return search(params, context, pluginManager);
		} catch (Exception e) {
			int numberOfErrors = Status.getNumberOfErrors() + 1;
			Status.setNumberOfErrors(numberOfErrors);
			logger.error("Error processing params", e);
		}		
		return null;			
	}

}
