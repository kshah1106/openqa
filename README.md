![Architecture](https://bitbucket.org/emarx/openqa/downloads/logo_openQA.png)

# Welcome

The use of Semantic Web technologies led to an increasing number of structured data published on the Web. 
Despite the advances on question answering systems retrieving the desired information from structured sources is 
still a substantial challenge. Users and researchers still face difficulties to integrate and compare their systems 
results and performance. *openQA* is an open source question answering framework that unifies approaches from several 
domain experts. The aim of *openQA* is to provide a common platform that can be used to promote advances by easy 
integration and measurement of different approaches. [more](https://bitbucket.org/emarx/openqa/wiki/Home)
##
[Project Website](http://openqa.aksw.org) | [Download](http://openqa.aksw.org/download.xhtml)
 | [FAQ](https://bitbucket.org/emarx/openqa/wiki/FAQ) | [License](https://tldrlegal.com/license/apache-license-2.0-%28apache-2.0%29#summary)

1) Add the following dependency on your project:

```
<dependency>
  <groupId>org.aksw.openqa</groupId>
  <artifactId>engine</artifactId>
  <version>0.0.7-beta</version>
</dependency>
```

2) Add the internal AKSW repository on your pom file:

```
<repositories>
    <repository>
      <id>maven.aksw.internal</id>
      <name>University Leipzig, AKSW Maven2 Repository</name>
      <url>http://maven.aksw.org/archiva/repository/internal</url>
    </repository>
  ...
</repositories>
```

3) Rock it.. ;-)

## NEWS ##

### 29/10/2015

The hoverboard arrived!!!
     
#### openQA engine v0.0.7-beta (hoverboard) Released

- openqa.engine.jar [download](https://bitbucket.org/emarx/openqa/downloads/openqa.engine-v0.0.7-beta.jar)

- openqa.engine.examples.jar [download](https://bitbucket.org/emarx/openqa/downloads/openqa.engine.examples-v0.0.7-beta.jar)

- openqa.engine.examples.par [download](https://bitbucket.org/emarx/openqa/downloads/openqa.engine.examples-v0.0.7-beta.par)

- plug-ins:
      + openqa.queryparser.sina.par [download](https://www.dropbox.com/s/1rz3j4i483u6hot/openqa.queryparser.sina-v0.0.7-beta.par)
      + openqa.queryparser.tbsl.par [download](https://www.dropbox.com/s/y9kbueranfevskc/openqa.queryparser.tbsl-v0.0.7-beta.par)
      + openqa.retriever.triplestore.par [download](https://www.dropbox.com/s/e3eqeddpu2068ts/openqa.retriever.triplestore-v0.0.7-beta.par)

##### Features

- ###### API enhancement

- ###### Full support for QALD benchmark

- ###### Support for Plugin ARchive (PAR) file package

- ###### Support for stack trace of the generated answers (runtime + pipeline)

- ###### Built-in JSON support

- ###### Support for different answer formulation pipelines

#### openQA Webserver full v0.0.7-beta (hoverboard) Released [download](https://www.dropbox.com/s/kyyg57mczol905v/openqa.webserver-v0.0.7-beta-full.war)

##### Features

- ###### API enhancement

- ###### Built in REST API

- ###### Enchacement of user managment

- ###### Interface enchacement

## Publications ##

[Towards an Open Question Answering Architecture, Semantics 2014](https://www.researchgate.net/publication/264412538_Towards_an_Open_Question_Answering_Architecture)