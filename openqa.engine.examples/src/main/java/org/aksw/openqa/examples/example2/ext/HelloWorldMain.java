package org.aksw.openqa.examples.example2.ext;

import org.aksw.openqa.AnswerFormulation;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.IResultMapList;
import org.aksw.openqa.examples.example2.HelloWorldQueryParser;
import org.aksw.openqa.examples.example2.HelloWorldRetriever;
import org.aksw.openqa.main.QueryResult;
import org.aksw.openqa.manager.plugin.PluginManager;

public class HelloWorldMain {

	/**
	 * This Example is an extension of Example 2 and shows how to print the stack trace 
	 * or the result in JSON format.
	 * 
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		String question = "";
		if(args.length > 0) {
			question  = args[0];
		}
		
		System.out.println("You:\t" + question);
		   	    	
    	HelloWorldQueryParser interpreter = new HelloWorldQueryParser();
    	HelloWorldRetriever retriever = new HelloWorldRetriever();
    	
    	PluginManager pluginManager = new PluginManager();
    	pluginManager.register(interpreter);
    	pluginManager.register(retriever);
    	pluginManager.setActive(true, interpreter.getId());
    	pluginManager.setActive(true, retriever.getId());
    	
    	AnswerFormulation queryProcessor = new AnswerFormulation(pluginManager);
    	
    	QueryResult result;
		result = queryProcessor.process(question);
		IResultMapList<? extends IResultMap> output = result.getOutput();
		
		System.out.println("openQA:\t");
		
		// printing stack trace		
		
		System.out.println("");
		System.out.println("Checking stack trace...");
		System.out.println("");
		System.out.println();
		output.printStackTrace();
		
		// printing JSON object
		
		System.out.println("");
		System.out.println("Printing result in a JSON format...");
		System.out.println("");
		System.out.println(output.toJSON());
	}

}
