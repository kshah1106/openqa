package org.aksw.openqa.examples.example1;

import java.util.List;

import org.aksw.openqa.Properties;
import org.aksw.openqa.AnswerFormulation;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.main.QueryResult;
import org.aksw.openqa.manager.plugin.PluginManager;

public class HelloWorldMain {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		String question = "";
		String answer = "";
		if(args.length > 0)
			question  = args[0];
		
		System.out.println("You:\t" + question);
		    	    	
    	HelloWorldQueryParser interpreter = new HelloWorldQueryParser();
    	
    	PluginManager pluginManager = new PluginManager();
    	pluginManager.register(interpreter);
    	pluginManager.setActive(true, interpreter.getId());
    	
    	AnswerFormulation queryProcessor = new AnswerFormulation(pluginManager);
    	
    	QueryResult result;
		result = queryProcessor.process(question);
		List<? extends IResultMap> output = result.getOutput();
		if(output.size() > 0)
			answer = output.get(0).getParam(Properties.Literal.TEXT, String.class);
		
		System.out.println("openQA:\t" + answer);		
	}
}
