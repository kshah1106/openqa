package org.aksw.openqa.component.answerformulation.retriever.impl;

import java.net.URI;
import java.util.Date;
import java.util.Map.Entry;
import java.util.Set;

import org.aksw.openqa.Properties;
import org.aksw.openqa.component.param.ResultMap;
import org.aksw.openqa.component.retriever.IRetrieverEntryInstanciator;
import org.aksw.openqa.component.retriever.PropertyEntry;
import org.aksw.openqa.component.retriever.ResultEntry;

import com.hp.hpl.jena.datatypes.BaseDatatype.TypedValue;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class EntryInstanciator implements IRetrieverEntryInstanciator<ResultMap, ResultEntry> {

	@Override
	public ResultMap newInstance(ResultEntry resultEntry) {
		ResultMap result = new ResultMap();
		Set<Entry<String, PropertyEntry>> propertiyMap = resultEntry.listProperties();
		for(Entry<String, PropertyEntry> property : propertiyMap) {
			PropertyEntry propertyEntry = property.getValue();
			Class<?> type = propertyEntry.getType();
			Object value = propertyEntry.getValue();
			if(type == URI.class) {
				result.setParam(Properties.URI, value.toString());
			} else if(type == Number.class) {
				result.setParam(Properties.Literal.NUMBER, value);
			} else if (type == Date.class) {
				result.setParam(Properties.Literal.DATE, value);
			} else if(type == Boolean.class) {
				result.setParam(Properties.Literal.BOOLEAN, value);
			} else if(TypedValue.class.isAssignableFrom(TypedValue.class)) {
				TypedValue typedValue = (TypedValue) value;
				result.setParam(Properties.Literal.TEXT, typedValue.lexicalValue);
			}			
		}		
		return result;
	}

	@Override
	public boolean stop() {
		return false;
	}
}
