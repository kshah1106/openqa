package org.aksw.openqa;

import java.util.List;

import org.aksw.openqa.component.ISpecializedPluginVisitor;
import org.aksw.openqa.component.answerformulation.IQueryParser;
import org.aksw.openqa.component.answerformulation.IRetriever;
import org.aksw.openqa.component.answerformulation.ISynthesizer;
import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.param.IParamMap;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.aksw.openqa.component.service.IService;
import org.aksw.openqa.main.ProcessResult;
import org.apache.log4j.Logger;

public class RunProcessVisitor extends AnswerFormulation implements ISpecializedPluginVisitor{
	
	private static Logger logger = Logger.getLogger(RunProcessVisitor.class);
	
	List<? extends IParamMap> params;
	ServiceProvider services;
	IContext context;
	ProcessResult result;
	
	public RunProcessVisitor(List<? extends IParamMap> params, ServiceProvider services, IContext context) {
		this.params = params;
		this.services = services;
		this.context = context;
	}
	
	@Override
	public void visit(IService service) {
		logger.warn("Services can not be runned.");
	}

	@Override
	public void visit(IQueryParser interpreter) {
		result = executeProcess(params, interpreter, services, context);
	}

	@Override
	public void visit(IRetriever retriever) {
		result = executeProcess(params, retriever, services, context);
	}

	@Override
	public void visit(ISynthesizer synthesizer) {
		result = executeProcess(params, synthesizer, services, context);
	}

	@Override
	public void visit(IContext context) {
		logger.warn("Context can not be runned.");
	}

	public ProcessResult getResult() {
		return result;
	}

}
