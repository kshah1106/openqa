package org.aksw.openqa.manager.plugin;

import java.io.File;
import java.io.FileFilter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.aksw.openqa.PersistentContextParams;
import org.aksw.openqa.component.IPlugin;
import org.aksw.openqa.component.IPluginVisitor;
import org.aksw.openqa.component.IProcess;
import org.aksw.openqa.component.IProvider;
import org.aksw.openqa.component.ISpecializedPluginVisitor;
import org.aksw.openqa.component.answerformulation.IQueryParser;
import org.aksw.openqa.component.answerformulation.IRetriever;
import org.aksw.openqa.component.answerformulation.ISynthesizer;
import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.providers.impl.ContextProvider;
import org.aksw.openqa.component.providers.impl.QueryParserProvider;
import org.aksw.openqa.component.providers.impl.RetrieverProvider;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.aksw.openqa.component.providers.impl.SynthesizerProvider;
import org.aksw.openqa.component.service.IService;
import org.aksw.openqa.util.PropertyLoaderUtil;
import org.aksw.openqa.util.ZIPUtil;
import org.apache.log4j.Logger;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class PluginManager extends PropertyLoaderUtil {
	
	private static Logger logger = Logger.getLogger(PluginManager.class);
	
	private final static String ACTIVE = "ACTIVE";
	
	private final static String CONTEXT_DIR = "context";
	
	private File pluginDir = null;
	private ClassLoader parent;
	
	ServiceProvider serviceProvider;
	QueryParserProvider queryParserProvider;
	RetrieverProvider retrieverProvider;
	SynthesizerProvider synthesizerProvider;
	ContextProvider contextProvider;
	PersistentContextParams persistentContextParams = null;
	
	RegisterVisitor registerVisitor = new RegisterVisitor();
	UnregisterVisitor unregisterVisitor = new UnregisterVisitor();
	
	Map<Class<? extends IProvider<? extends IPlugin>>, 
			IProvider<? extends IPlugin>> providers = new HashMap<Class<? extends IProvider<? extends IPlugin>>, 
															IProvider<? extends IPlugin>>();
	
	public PluginManager(String pluginDir, ClassLoader parent, String appContext, IPlugin... plugins) {
		this.parent = parent;
		this.pluginDir = new File(pluginDir);
		File contextDir = new File(pluginDir + "/" + CONTEXT_DIR);
		if(!contextDir.exists()) {
			contextDir.mkdir();
		}
		this.persistentContextParams = new PersistentContextParams(pluginDir + "/" + CONTEXT_DIR, appContext);
		load();
		for (IPlugin plugin : plugins) {
			unregister(plugin);
			register(plugin);
		}
	}
	
	public PluginManager(String plugginDir, ClassLoader parent, String appContext) {
		this.parent = parent;
		this.pluginDir = new File(plugginDir);
		File contextDir = new File(plugginDir + "/" + CONTEXT_DIR);
		if(!contextDir.exists()) {
			contextDir.mkdir();
		}
		this.persistentContextParams = new PersistentContextParams(plugginDir + "/" + CONTEXT_DIR, appContext);
		load();
	}
	
	public PluginManager(String plugginDir, ClassLoader parent) {
		this.parent = parent;
		this.pluginDir = new File(plugginDir);
		load();
	}
	
	public PluginManager(ClassLoader parent, String appContext) {		 
		this.parent = parent;
        this.persistentContextParams = new PersistentContextParams(appContext);
        load();
	}
	
	public PluginManager(ClassLoader parent) {
		this.parent = parent;
		load();
	}
	
	public PluginManager(String appContext) {		 
		this.parent = Thread.currentThread().getContextClassLoader();
        this.persistentContextParams = new PersistentContextParams(appContext);
        load();
	}
	
	public PluginManager() {        
        this.parent = Thread.currentThread().getContextClassLoader();
        load();
	}
	
	private void deploy() {
		if (pluginDir != null) {
			File[] directories = pluginDir.listFiles(new FileFilter() {
				@Override
				public boolean accept(File file) {
					return file.isDirectory();
				}
			});
			List<String> directoryNames = new ArrayList<String>();
			for(File dir : directories) {
				directoryNames.add(dir.getAbsolutePath() + ".par");
			}			

			File[] parFiles = pluginDir.listFiles(new FileFilter() {
				@Override
				public boolean accept(File file) {
					return file.getName().endsWith(".par");
				}
			});
			List<String> parFileNameList = new ArrayList<String>();
			for(File parFile : parFiles) {
				String absolutePath = parFile.getAbsolutePath();
				parFileNameList.add(absolutePath);
			}
			
			parFileNameList.removeAll(directoryNames); // removing all existing deployed directories
			
		    for(String parFile : parFileNameList) {
		    	String destDirName = parFile.substring(0, parFile.length() - 4);
		    	File destDir = new File(destDirName);
		    	destDir.mkdir(); // create dir since it does not exists anyways
		    	ZIPUtil.unzip(parFile, destDir.getAbsolutePath()); // deploy
		    }
		}
	}
	
	private void load() {
		logger.debug("Deploying existing PAR files...");
		deploy(); // deploy existing undeployed par files		

		logger.debug("Loading plug-ins...");
        List<ClassLoader> classLoaders = new ArrayList<ClassLoader>();
        
        // add the application directory as class path to look for plugins in main class path
        classLoaders.add(parent);
        
        if(pluginDir != null) {
	        File[] directories = pluginDir.listFiles(new FileFilter() {
	          @Override
	          public boolean accept(File file) {
	            return file.isDirectory();
	          }
	        });
	        
	        if(directories != null) {
		        for(File directory : directories) {
		           File[] flist = directory.listFiles(new FileFilter() {
		                public boolean accept(File file) {return file.getPath().toLowerCase().endsWith(".jar");}
		           });
		
		           URL[] urls = new URL[flist.length];
		           for (int i = 0; i < flist.length; i++) {
		        	   try {
							urls[i] = flist[i].toURI().toURL();
		        	   } catch (MalformedURLException e) {
							logger.error("Error in URI format", e);
		        	   }
		           }
		           
		           if(urls.length > 0 ) {
		        	   URLClassLoader ucl = new URLClassLoader(urls, parent);
		        	   classLoaders.add(ucl);
		           }
		        }
	        }
        }
        
        serviceProvider = new ServiceProvider(classLoaders);
        providers.put(ServiceProvider.class, serviceProvider);
        
        queryParserProvider = new QueryParserProvider(classLoaders, serviceProvider);
        providers.put(QueryParserProvider.class, queryParserProvider);
        
        retrieverProvider = new RetrieverProvider(classLoaders, serviceProvider);
        providers.put(RetrieverProvider.class, retrieverProvider);
        
        synthesizerProvider = new SynthesizerProvider(classLoaders, serviceProvider);
        providers.put(SynthesizerProvider.class, synthesizerProvider);        
               
        contextProvider = new ContextProvider(classLoaders, serviceProvider);
        providers.put(ContextProvider.class, contextProvider);
        
        // load persistent context params
        if(persistentContextParams != null) {
        	visit(new ContexParamVisitor());
        }
	}
    
    public void register(IPlugin plugin) {
    	plugin.accept(registerVisitor);
    }
    
    public void unregister(IPlugin plugin) {
    	plugin.accept(unregisterVisitor);
    }
    
    public void setPluginParam(String pluginID, Map<String, Object> params) {
    	visit(new SetParamVisitor(pluginID, params));
	}
    
    public void saveState() {
    	visit(new SaveStateVisitor());
    }
    
    public void saveState(String pluginID) {
    	visit(new SaveStateVisitor(pluginID));
    }
    
    /**
	 * Set the activation value to all plug-ins.
	 * 
	 * @param active true to activate all loaded plug-ins or false otherwise. 
	 */
	public void setActive(boolean active) {
		setActive(active, (String[])null);
	}
	
	/**
	 * Shutdown all plug-ins.
	 */
	public void shutdown() {
		visit(new PluginShutdownVisitor((String[])null));
	}
	
	/**
	 * Set the activation to all plug-ins.
	 * 
	 * @param active true to activate all loaded plug-ins or false otherwise.
	 * @param ids plug-in list id's to be activated or deactivated.
	 */
	public void setActive(boolean active, String... ids) {
		visit(new PluginActivateVisitor(active, ids));
	}
        
    public List<? extends IPlugin> getPlugins(Class<? extends IProvider<? extends IPlugin>> providerClass) throws Exception {
    	Map<Class<? extends IProvider<? extends IPlugin>>, IProvider<? extends IPlugin>> providers = getProviders();
    	IProvider<? extends IPlugin> provider = providers.get(providerClass);
    	return provider.list();
    }
    
    public IPlugin getPlugin(String id)  {    	
    	Map<Class<? extends IProvider<? extends IPlugin>>, IProvider<? extends IPlugin>> providers = getProviders();
    	for(IProvider<? extends IPlugin> providerEntry : providers.values()) {
    		IPlugin component = providerEntry.get(id);
    		if(component != null) {
    			return component;
    		}
    	}    	
    	return null;
    }
    
    public <C extends IPlugin> C getPlugin(Class<? extends IProvider<?>> providerClass, Class<C> componentClass) {
    	Map<Class<? extends IProvider<? extends IPlugin>>, IProvider<? extends IPlugin>> providers = getProviders();
    	@SuppressWarnings("unchecked")
		IProvider<C> provider = (IProvider<C>) providers.get(providerClass);
    	if(provider != null) {
			return provider.get(componentClass);
    	}
    	return null;
    }
    
    public <L extends IProvider<? extends IPlugin>> L getProvider(Class<L> providerClass) {
    	Map<Class<? extends IProvider<? extends IPlugin>>, IProvider<? extends IPlugin>> providers = getProviders();
		@SuppressWarnings("unchecked")
		L provider = (L) providers.get(providerClass);
		return provider;
    }
    
    public IProcess getProcessProvider(String componentID) {
    	if(queryParserProvider.getId().equals(componentID)) {
    		return queryParserProvider;
    	} else if(retrieverProvider.getId().equals(componentID)) {
    		return retrieverProvider;
    	} else if(synthesizerProvider.getId().equals(componentID)) {
    		return synthesizerProvider;
    	} 
    	return null;
    }
    
	private Map<Class<? extends IProvider<? extends IPlugin>>, IProvider<? extends IPlugin>> getProviders() {
		return providers;
	}

	
	/**
	 * Visit all plug-ins in the classpath active and inactive.
	 * 
	 * @param visitor visitor of the plug-ins
	 */
	public void visit(IPluginVisitor visitor) {
		Map<Class<? extends IProvider<? extends IPlugin>>, IProvider<? extends IPlugin>> providersMap = getProviders();
		Collection<IProvider<? extends IPlugin>> providers = providersMap.values();
		for(IProvider<? extends IPlugin> provider : providers) {
			List<? extends IPlugin> plugins = provider.list();
			for(IPlugin plugin : plugins) {
					plugin.accept(visitor);
			}
		}
	}
	
	private class RegisterVisitor implements ISpecializedPluginVisitor {
    	
    	public void visit(IService service) {
    		serviceProvider.register(service);
    	}
    	
    	public void visit(IQueryParser interpreter) {
    		queryParserProvider.register(interpreter);
    	}
    	
    	public void visit(IRetriever retriever) {
    		retrieverProvider.register(retriever);
    	}
    	
    	public void visit(ISynthesizer synthesizer) {
    		synthesizerProvider.register(synthesizer);
    	}
    	
    	public void visit(IContext context) {
    		contextProvider.register(context);
    	}
    	
    }
    
    private class UnregisterVisitor implements ISpecializedPluginVisitor {
    	
    	public void visit(IService service) {
    		serviceProvider.unregister(service);
    	}
    	
    	public void visit(IQueryParser interpreter) {
    		queryParserProvider.unregister(interpreter);
    	}
    	
    	public void visit(IRetriever retriever) {
    		retrieverProvider.unregister(retriever);
    	}
    	
    	public void visit(ISynthesizer synthesizer) {
    		synthesizerProvider.unregister(synthesizer);
    	}
    	
    	public void visit(IContext context) {
    		contextProvider.unregister(context);
    	}    	
    }
    
    private class ContexParamVisitor implements IPluginVisitor {
		public void visit(IPlugin plugin) {
    		Map<String, String> params = persistentContextParams.listParams(plugin.getId());
    		HashMap<String, Object> newMap = new HashMap<String, Object>();
    		newMap.putAll(params);
    		newMap.remove(ACTIVE);
    		plugin.setProperties(newMap);
    		String active = (String) params.get(ACTIVE);
    		if(active != null) {
    			plugin.setActive(active.equals("true"));
    		} else {
    			plugin.setActive(true);
    		}
    	}
    }
    
    private class SaveStateVisitor implements IPluginVisitor {
    	String pluginID;
    	
    	public SaveStateVisitor() {
		}
    	
    	public SaveStateVisitor(String pluginID) {
    		this.pluginID = pluginID;
    	}
    	
		public void visit(IPlugin plugin) {
			if(pluginID == null || pluginID.equals(plugin.getId())) {
				Map<String, Object> params = plugin.getParameters();
				for(Entry<String, Object> e : params.entrySet()) {
					persistentContextParams.setProperty(plugin.getId(), e.getKey(), e.getValue().toString());
				}
				persistentContextParams.setProperty(plugin.getId(), ACTIVE, plugin.isActive()?"true":"false");
			}
    	}    	
    }
    
    private class SetParamVisitor implements IPluginVisitor {
    	String pluginID;
    	Map<String, Object> params;
    	
    	public SetParamVisitor(String pluginID, Map<String, Object> params) {
    		this.pluginID = pluginID;
    		this.params = params;
    	}
    	
		public void visit(IPlugin plugin) {
			if(pluginID != null && pluginID.equals(plugin.getId())) {
				for(Entry<String, Object> e : params.entrySet()) {
					plugin.setParam(e.getKey(), e.getValue());
				}
			}
    	}
    }
    
    private class PluginActivateVisitor implements IPluginVisitor {
    	String[] pluginIDs;
    	Boolean activate;
    	
    	public PluginActivateVisitor(boolean activate, String... pluginIDs) {
    		this.activate = activate;
    		this.pluginIDs = pluginIDs;
    	}
    	
		public void visit(IPlugin plugin) {
			List<String> idList = null;
			if(pluginIDs != null) {
				idList = Arrays.asList(pluginIDs);
			}
			if(idList == null || idList.contains(plugin.getId())) {
				plugin.setActive(activate);				
			}
    	}    	
    }
    
    private class PluginShutdownVisitor implements IPluginVisitor {
    	String[] pluginIDs;
    	
    	public PluginShutdownVisitor(String... pluginIDs) {
    		this.pluginIDs = pluginIDs;
    	}
    	
		public void visit(IPlugin plugin) {
			List<String> idList = null;
			if(pluginIDs != null) {
				idList = Arrays.asList(pluginIDs);
			}
			if(idList == null || idList.contains(plugin.getId())) {
				plugin.shutdown();				
			}
    	}    	
    }
}