package org.aksw.openqa;

import java.util.Set;

public class Stage {
	String id;
	Set<String> componentIDs;
	
	public Set<String> getComponentIDs() {
		return componentIDs;
	}
	
	public void setComponentIDs(Set<String> componentIDs) {
		this.componentIDs = componentIDs;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
