package org.aksw.openqa.qald;

public class QuestionResult {
	
	Long runtime;
	Double fmeasure;
	Double precison;
	Double recal;
	Exception exception;
	
	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public long getRuntime() {
		return runtime;
	}
	
	public void setRuntime(long runtime) {
		this.runtime = runtime;
	}
	
	public double getFmeasure() {
		return fmeasure;
	}
	
	public void setFmeasure(double fmeasure) {
		this.fmeasure = fmeasure;
	}
	
	public double getPrecision() {
		return precison;
	}
	
	public void setPrecison(double precison) {
		this.precison = precison;
	}
	
	public double getRecall() {
		return recal;
	}
	
	public void setRecal(double recal) {
		this.recal = recal;
	}
	
	@Override
	public String toString() {
		return "Recall:" + getRecall() +
				" Precision:" + getPrecision() +
				" F-measure:" + getFmeasure();
	}
}
