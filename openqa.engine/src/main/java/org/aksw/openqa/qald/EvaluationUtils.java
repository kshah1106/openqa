package org.aksw.openqa.qald;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.aksw.openqa.qald.schema.Answer;
import org.aksw.openqa.qald.schema.Question;

public class EvaluationUtils {

	public static double precision(Question expected, Question actual) {
		double precision = 0;
		List<Answer> targetAnswerList = expected.getAnswers().getAnswer();
		List<Answer> givenAnswerList = actual.getAnswers().getAnswer();
		
		if(targetAnswerList.size() == 0 
				&& givenAnswerList.size() == 0) {
			return 1;
		}
		
		Set<Answer> intersectionAnswerSet = new HashSet<Answer>(targetAnswerList);
		Set<Answer> givenAnswerSet = new HashSet<Answer>(givenAnswerList);
		
		intersectionAnswerSet.retainAll(givenAnswerSet); // calculating the intersection
		
		if(givenAnswerSet.size() == 0) {
			return 0;
		}
		
		precision = (double) intersectionAnswerSet.size() / (double) givenAnswerSet.size();
		
		return precision;
	}

	public static double recall(Question expected, Question actual) {
		double precision = 0;
		List<Answer> targetAnswerList = expected.getAnswers().getAnswer();
		List<Answer> givenAnswerList = actual.getAnswers().getAnswer();
		
		if(targetAnswerList.size() == 0 
				&& givenAnswerList.size() == 0) {
			return 1;
		}
		
		Set<Answer> expectedAnswerSet = new HashSet<Answer>(targetAnswerList);
		Set<Answer> intersectionAnswerSet = new HashSet<Answer>(targetAnswerList);
		Set<Answer> actualAnswerSet = new HashSet<Answer>(givenAnswerList);
		
		intersectionAnswerSet.retainAll(actualAnswerSet); // calculating the intersection
		
		if(expectedAnswerSet.size() == 0) {
			return 0;
		}
	
		precision = (double) intersectionAnswerSet.size() / (double) expectedAnswerSet.size();
		
		return precision;
	}

	public static double fMeasure(Question expected, Question actual) {
		double precision = precision(expected, actual);
		double recall = recall(expected, actual);
		double fMeasure = 0;
		if (precision + recall > 0) {
			fMeasure = 2 * precision * recall / (precision + recall);
		}
		return fMeasure;
	}

}