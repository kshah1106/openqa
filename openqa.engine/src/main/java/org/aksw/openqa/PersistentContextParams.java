package org.aksw.openqa;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentNavigableMap;

import org.mapdb.DB;
import org.mapdb.DBMaker;

public class PersistentContextParams implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8598003281170345448L;
	File file;
	
	public PersistentContextParams(String appContext) {
		file = new File(appContext + "_custom.prop");				
	}
	
	public PersistentContextParams(String path, String appContext) {
		file = new File(path + "/" + appContext + "_custom.prop");
	}
	
	public synchronized String getProperty(String context, String property, String defaultValue) {
		DB db = DBMaker.newFileDB(file).make();
		ConcurrentNavigableMap<String, String> treeMap = db.getTreeMap(context);
		String value = treeMap.get(property);
		if(value == null) {
			return defaultValue;
		}
		db.close();
		return value;			
	}
	
	public Map<String, String> listParams(String componentContext) {
		Map<String, String> params = new HashMap<String, String>();
		DB db = DBMaker.newFileDB(file).make();
		db = DBMaker.newFileDB(file).make();
		ConcurrentNavigableMap<String, String> treeMap = db.getTreeMap(componentContext);
		for(Entry<String, String> entry : treeMap.entrySet()) {
			params.put(entry.getKey(), entry.getValue());
		}
		db.close();
		return params;
	}
	
	public synchronized void setProperty(String context, String property, String value) {
		DB db = DBMaker.newFileDB(file).make();
		ConcurrentNavigableMap<String, String> treeMap = db.getTreeMap(context);
		treeMap.put(property, value);
		db.commit();
		db.close();
	}
}
