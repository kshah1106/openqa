package org.aksw.openqa;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class DefaultPipeline extends ArrayList<Stage> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4387911617483672688L;

	public DefaultPipeline() {
		Stage stage0 = new Stage();
		Set<String> componentIDs = new HashSet<String>();
		stage0.setId("QueryParser");
		componentIDs.add("QueryParserProvider");
		stage0.setComponentIDs(componentIDs);		
		add(stage0);
		
		Stage stage1 = new Stage();
		stage1.setId("Retriever");
		componentIDs = new HashSet<String>();
		componentIDs.add("RetrieverProvider");		
		stage1.setComponentIDs(componentIDs);
		add(stage1);
		
		Stage stage2 = new Stage();
		stage2.setId("Synthesizer");
		componentIDs = new HashSet<String>();
		componentIDs.add("SynthesizerProvider");		
		stage2.setComponentIDs(componentIDs);
		add(stage2);
	}

}
