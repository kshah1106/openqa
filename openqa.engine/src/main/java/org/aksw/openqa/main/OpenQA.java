package org.aksw.openqa.main;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.aksw.openqa.AnswerFormulation;
import org.aksw.openqa.component.IPlugin;
import org.aksw.openqa.component.IPluginVisitor;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.manager.plugin.PluginManager;
import org.aksw.openqa.qald.QALDBenchmark;
import org.aksw.openqa.qald.QALDBenchmarkResult;
import org.aksw.openqa.util.ZIPUtil;
import org.apache.log4j.Logger;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class OpenQA {
	
	private static Logger logger = Logger.getLogger(OpenQA.class);
	
	public static final String ENGINE_VERSION = "v0.1.7-beta";
	
	public static final String API_VERSION = "1.0-beta";
	
	public static void main(String[] args) throws Exception {
		if(args.length < 2) {
			printHelp();
			return;
		}

		String args0 = args[0];
		if((args0.charAt(0) == '/') || (args0.charAt(0) == '\\')) {
			args0 = args0.substring(1, args0.length());
		}
		
		File pluginDir = new File(args0);
		
		if(!pluginDir.isDirectory()) {
			printHelp();
			return;
		}
		
		OpenQA openQA = new OpenQA();
		
		ClassLoader contextClassLoader = openQA.getClass().getClassLoader();
		PluginManager pluginManager = new PluginManager(pluginDir.getAbsolutePath(), contextClassLoader);
    	
    	String[] activateList = openQA.getActivateList(args);
    	if(activateList == null) {
    		pluginManager.setActive(true);
    	} else {
    		pluginManager.setActive(true, activateList);
    	}
    	
    	String arg1 = args[1];
    	if(arg1.equals("-version")) {
    		printVersion(ENGINE_VERSION, API_VERSION);
    		return;
    	} if(arg1.equals("-list") && (args.length == 2)) {
			printPlugins(pluginManager);
			return;
		} else if (arg1.equals("-info") && (args.length == 3)) {
			printInfo(pluginManager, args[2]);
			return;
		} else if(arg1.equals("-query") && (args.length == 3 || args.length == 5)) {
			openQA.answer(args[2], pluginManager);
			return;
		} else if(arg1.equals("-qald") && (args.length == 6 || args.length == 8)) {
			String arg2 = args[2];
			File qaldFile = new File(arg2);
			File outputFile = new File(args[3]);
			if(qaldFile.isFile() && outputFile.isFile()) {
		    	if(contains(args, "-answer")) {	    		
		    		QALDBenchmark qaldBenchmark = new QALDBenchmark();
		    		qaldBenchmark.evaluate(qaldFile, outputFile, args[4], args[5], pluginManager);
		    	}
		    	File systemAnswersFile = new File(args[3]);
	    		QALDBenchmarkResult qaldBenchmarkResult = QALDBenchmark.evaluate(qaldFile, systemAnswersFile);
	    		System.out.println(qaldBenchmarkResult.toString());
				return;
			}
		} else if(arg1.equals("-par") && (args.length == 3)) {
			File targetPlugin = new File(args[0] + "/" + args[2]);
			String pluginPath = targetPlugin.getAbsolutePath();
			if(targetPlugin.exists() && targetPlugin.isDirectory()) {
				String pluginFileName =  args[2] + ".par";
				System.out.println("Generating PAR for " + args[2]);
				ZIPUtil.zip(pluginPath, pluginFileName); // generates the par (Plug-in Archive) file
				System.out.println("PAR " + pluginFileName + " generated with success.");
				return;
			}
		}
    	printHelp();    	
	}
	
	public void answer(String question, PluginManager pluginManager) {
		logger.debug("Processing query: " + question);
		AnswerFormulation queryProcessor = new AnswerFormulation();
		QueryResult result;
		try {
			result = queryProcessor.process(question, pluginManager);
			List<? extends IResultMap> output = result.getOutput();
			String array = "";
			if(output != null && output.size() > 0) {
				array += "[";
				for(IResultMap outputEntry : output) {
					Map<String, Object> attrs = outputEntry.getParameters();
					array += "{"; // starting element
					String elementAttr = "";
					for(Entry<String, Object> attrEntry : attrs.entrySet()) {
						elementAttr += "\"" + attrEntry.getKey() + "\": \"" + attrEntry.getValue() + "\", ";
					}
					elementAttr = elementAttr.substring(0, elementAttr.length()-2); // remove last comma
					array += elementAttr + "}, "; // finishing element
				}
				array = array.substring(0, array.length()-2);
				array += "]";
				System.out.println(array);
			}
		} catch (Exception e) {
			logger.error("Error processing query: " + question, e);
		}
	}
	
	private static void printInfo(PluginManager pluginManager, final String pluginID) throws Exception {
		System.out.println("Info: ");
		pluginManager.visit(new IPluginVisitor() {
			@Override
			public void visit(IPlugin plugin) {
				print(plugin, pluginID);
			}
		});
	}
	
	private static void printPlugins(PluginManager pluginManager) throws Exception {
		System.out.println("Plugins: ");
		pluginManager.visit(new IPluginVisitor() {			
			@Override
			public void visit(IPlugin context) {
				print(context);
			}
		});
	}

	private static void print(IPlugin plugin) {
		System.out.println("    " + plugin.getId());
	}
	
	private static void print(IPlugin plugin, String pluginID) {
		if(pluginID.equals(plugin.getId())) {
			System.out.println("id: \t\t " + plugin.getId());
			System.out.println("label: \t\t " + plugin.getLabel());
			System.out.println("version: \t " + plugin.getVersion());
			System.out.println("API: \t\t " + plugin.getAPI());
			System.out.println("author: \t " + plugin.getAuthor());
			System.out.println("contact: \t " + plugin.getContact());
			System.out.println("website: \t " + plugin.getWebsite());
			System.out.println("license: \t " + plugin.getLicense());
			System.out.println("description: \t " + plugin.getDescription());
			System.out.println("input: \t\t " + plugin.getInput());
			System.out.println("output: \t " + plugin.getOutput());
		}
	}

	public String[] getActivateList(String[] args) {
		List<String> argumentList = Arrays.asList(args);
		int activateIndx = argumentList.indexOf("-activate");
		if(activateIndx == -1 || activateIndx+2 > argumentList.size()) {
			return null;
		}
		
		String activeStringList = argumentList.get(activateIndx+1);
		return activeStringList.split(",");
	}
	
	public static int indxOf(String[] args, String param) {
		List<String> argumentList = Arrays.asList(args);
		int indx = argumentList.indexOf(param);
		return indx;
	}
	
	public static boolean contains(String[] args, String param) {
		int indx = indxOf(args, param);
		return indx > 0; 
	}
	
	public static void printVersion(String engineVersion, String apiVersion) {
		System.out.println("openQA version " + engineVersion + " API " + apiVersion);
	}
	
	public static void printHelp() {
		System.out.println("Use java -jar openqa.engine.jar <plug-insDir> [option]");
		System.out.println("Where [option] is:");
		System.out.println("   * -info  <plug-inID>   - show the plug-in info");
		System.out.println("   * -list  - list all plug-ins in the given plug-ins directory");
		System.out.println("   * -query <query> [-activate <activateList>]");
		System.out.println("       e.g. java -jar openqa.engine.jar \\pluginsDir \"How to run openQA in a standalone fashion?\" -activate \"plug-inId1, plug-inId2\"");
		System.out.println("   * -par <plug-inDir> - generates the PAR (Plug-in Archive) of the given plug-in directory.");
		System.out.println("       e.g. java -jar openqa.engine.jar \\pluginsDir openqa.queryparser.sina");
		System.out.println("   * -qald -answer <QALDTestFile> <outputAnswerFile> <lang> <QALDQueryType> [-activate <activateList>]");
		System.out.println("       e.g. java -jar openqa.engine.jar \\pluginsDir -qald -answer QALD4.xml result.xml en -activate \"plug-inId1, plug-inId2\"");
		System.out.println("   * -qald -evaluate <QALDAnswerFile> <systemAnswerFile>");
		System.out.println("       e.g. java -jar openqa.engine.jar \\pluginsDir -qald -evaluate QALD4withAnswers.xml result.xml");
		System.out.println("   * -version - display the engine version.");
		System.out.println("Dictionary: ");
		System.out.println("  plug-insDir		-	directory containing the plug-ins.");
		System.out.println("  plug-inDir	    -	directory of the plug-in.");
		System.out.println("  query		        -	a cotated query (keywords or full question) e.g.\"Who was the discoverer of Brazil?\".");
		System.out.println("  QALDTestFile		-   QALD test file.");
		System.out.println("  QALDAnswerFile	-   QALD test file with answers.");
		System.out.println("  QALDQueryType	    -   QALD query type (keywords,string).");
		System.out.println("  outputFile		-	destination output file.");
		System.out.println("  lang			    -	language to test (en,de,es,it,fr,nl,ro).");
		System.out.println("  -activate		    -	list of plug-ins to be activated splited by comma (optional). e.g \"plug-inId1, plug-inId2\".");
	}
}