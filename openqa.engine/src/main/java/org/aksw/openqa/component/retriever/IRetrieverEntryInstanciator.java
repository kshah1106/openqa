package org.aksw.openqa.component.retriever;

public interface IRetrieverEntryInstanciator <T, L> {
	T newInstance(L entry);
	boolean stop();
}
