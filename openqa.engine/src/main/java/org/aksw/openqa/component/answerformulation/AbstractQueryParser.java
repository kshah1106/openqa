package org.aksw.openqa.component.answerformulation;

import java.util.Map;

import org.aksw.openqa.component.AbstractQFProcessor;
import org.aksw.openqa.component.ISpecializedPluginVisitor;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public abstract class AbstractQueryParser extends AbstractQFProcessor implements IQueryParser {

	public AbstractQueryParser(Map<String, Object> params) {
		super(params);
	}
		
	public void accept(ISpecializedPluginVisitor visitor) {
		visitor.visit(this);
	}
}
