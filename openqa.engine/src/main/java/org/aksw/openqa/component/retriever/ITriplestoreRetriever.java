package org.aksw.openqa.component.retriever;

import java.util.List;

import org.aksw.openqa.component.IPlugin;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 * @param <L>
 */
public interface ITriplestoreRetriever <L> extends IPlugin {
	
	public <T> List<T> retrieve(String sparqlQuery, IRetrieverEntryInstanciator<T, L> instanciator);
	public <T> List<T> retrieve(String sparqlQuery, String graph, String endpoint, IRetrieverEntryInstanciator<T, L> instanciator);
	public <T> List<T> retrieve(String sparqlQuery, String graph, String endpoint, long readTimeOut, long connectionTimeOut, IRetrieverEntryInstanciator<T, L> instanciator);

}
