package org.aksw.openqa.component.context.impl;

import java.util.Map;

import org.aksw.openqa.component.context.AbstractContext;
import org.aksw.openqa.main.OpenQA;

public class Context extends AbstractContext {

	String inputQuery;
	
	public Context(Map<String, Object> params) {
		super(params);
	}

	@Override
	public String getInputQuery() {
		return inputQuery;
	}
	
	public void setInputQuery(String inputQuery) {
		this.inputQuery = inputQuery;
	}
	
	@Override
	public String getVersion() {
		return OpenQA.ENGINE_VERSION;
	}
	
	@Override
	public String getAPI() {
		return OpenQA.API_VERSION;
	}
}
