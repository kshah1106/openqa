package org.aksw.openqa.component.param;

import java.util.ArrayList;
import java.util.List;

public class ResultMapList<E extends IResultMap> extends ArrayList<E> implements IResultMapList<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3472859946054729838L;
	List<E> notProcessed;

	@Override
	public String toJSON() {
		JSONResultVisitor vs = new JSONResultVisitor();
		this.accept(vs);
		return vs.getJSON();
	}

	@Override
	public void printStackTrace() {
		PrintStackTraceResultVisitor ps = new PrintStackTraceResultVisitor();
		this.accept(ps);
	}

	@Override
	public void accept(IMapVisitor visitor) {
		visitor.visit(this);
	}

}
