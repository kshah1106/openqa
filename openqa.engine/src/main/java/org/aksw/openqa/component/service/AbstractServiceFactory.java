package org.aksw.openqa.component.service;

import org.aksw.openqa.component.AbstractPluginFactory;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public abstract class AbstractServiceFactory extends AbstractPluginFactory<IService> implements IServiceFactory {
}
