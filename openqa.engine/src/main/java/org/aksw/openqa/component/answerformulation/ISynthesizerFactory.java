package org.aksw.openqa.component.answerformulation;

import org.aksw.openqa.component.IPluginFactorySpi;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public interface ISynthesizerFactory extends IPluginFactorySpi<ISynthesizer> {

}
