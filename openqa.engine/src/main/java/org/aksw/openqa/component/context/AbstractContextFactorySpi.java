package org.aksw.openqa.component.context;

import org.aksw.openqa.component.AbstractPluginFactory;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public abstract class AbstractContextFactorySpi extends AbstractPluginFactory<IContext> implements IContextFactory {
}
