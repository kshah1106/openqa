package org.aksw.openqa.component.param;

import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONResultVisitor implements IMapVisitor {
	
	String json = "";

	@Override
	public void visit(IParamMap params) {
		if(params != null) {
			Map<String, Object> sourceParams = params.getParameters();	
			if (sourceParams != null) {
				JSONObject object = new JSONObject();
				for (Entry<String, Object> param : sourceParams.entrySet()) {					
					try {
						object.put(param.getKey(), param.getValue());
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				json = object.toString();
			}
		}
	}

	@Override
	public void visit(IResultMap params) {
		visit((IParamMap)params);
	}

	@Override
	public void visit(IResultMetaInfo params) {
		visit((IParamMap)params);
	}
	
	/**
	 * Return the IParam as a JSON object serialized as String.
	 * 
	 * @return the IParam as a JSON object serialized as String.
	 */
	public String getJSON() {
		return json;
	}

	@Override
	public void visit(IResultMapListMetaInfo<? extends IResultMap> params) {
		visit((IResultMapList<? extends IResultMap>)params);
	}

	@Override
	public void visit(IResultMapList<? extends IResultMap> params) {
		JSONArray array = new JSONArray();
		for(IResultMap r : params) {
			r.accept(this);
			JSONObject o;
			try {
				o = new JSONObject(json);
				array.put(o);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		json = array.toString();
	}

}
