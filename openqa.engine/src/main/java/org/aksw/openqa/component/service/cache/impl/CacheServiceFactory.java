package org.aksw.openqa.component.service.cache.impl;

import java.util.Map;

import org.aksw.openqa.component.service.AbstractServiceFactory;
import org.aksw.openqa.component.service.IService;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class CacheServiceFactory extends AbstractServiceFactory {
	@Override
	public IService create(Map<String, Object> params) {
		return create(CacheService.class, params);
	}
}
