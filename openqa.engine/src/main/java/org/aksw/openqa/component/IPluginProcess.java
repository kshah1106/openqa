package org.aksw.openqa.component;

import java.util.List;

import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.param.IParamMap;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.providers.impl.ServiceProvider;

public interface IPluginProcess extends IPlugin, IProcess {
	public boolean canProcess(List<? extends IParamMap> params);
	public boolean canProcess(IParamMap param);
	public List<? extends IResultMap> process(IParamMap param, ServiceProvider services, IContext context) throws Exception;
}