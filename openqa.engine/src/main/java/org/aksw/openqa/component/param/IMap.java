package org.aksw.openqa.component.param;

import java.util.Map;

public interface IMap {
	public Object getParam(String property);
	public Object getParam(String property, Object defaultValue);
	public <T> T getParam(String property, Class<T> clazz);
	public <T> T getParam(String property, Class<T> clazz, T defaultValue);
	public void setParam(String attr, Object value);
	
	public Map<String, Object> getParameters();
	public boolean contains(String property);
	public boolean contains(String property, Class<?> clazz);
}
