package org.aksw.openqa.component.answerformulation;

import org.aksw.openqa.component.AbstractPluginFactory;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public abstract class AbstractResolverFactory extends AbstractPluginFactory<IResolver> implements IResolverFactory {
}
