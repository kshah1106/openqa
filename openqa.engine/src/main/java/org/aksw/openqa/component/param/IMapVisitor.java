package org.aksw.openqa.component.param;

public interface IMapVisitor {
	void visit(IParamMap params);
	
	void visit(IResultMap params);
	
	void visit(IResultMetaInfo params);
	
	void visit(IResultMapListMetaInfo<? extends IResultMap> params);
	
	void visit(IResultMapList<? extends IResultMap> params);
}
